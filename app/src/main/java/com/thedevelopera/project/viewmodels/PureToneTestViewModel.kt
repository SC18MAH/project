package com.thedevelopera.project.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.thedevelopera.project.utilities.ToneThread
import com.thedevelopera.project.data.HearingResult

import com.thedevelopera.project.data.HearingResultRepository
import com.thedevelopera.project.data.TestingResult
import com.thedevelopera.project.data.TestingResultRepository
import org.jetbrains.anko.doAsync
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.abs
import kotlin.math.pow

class PureToneTestViewModel internal constructor(private val testingResultRepository: TestingResultRepository,
                                                 private val hearingResultRepository: HearingResultRepository): ViewModel(){

    private var testType : String = "Pure Tone Audiometry"
    private var toneThread : ToneThread? = null
    private val testingFrequencies = intArrayOf(1000, 2000, 4000, 6000, 8000,500,250)
    private val conversionSPLtoHL = doubleArrayOf(7.5, 9.0, 12.0, 16.0, 15.5, 13.5, 27.0)
    val toastMessage = MutableLiveData<String>()
    val isTestDone = MutableLiveData<Boolean>()
    private val date = Calendar.getInstance()
    var isBarelyAudible = false
    //    Int used instead of Boolean as cannot make it null, so using int, -1 = Null, 0 = No, 1 = Yes
    var isHeard: Int = -1
    var isResponseMade = false
    private val hearingResultList = ArrayList<HearingResult>()
    val tvText = MutableLiveData<String>()
    private var isFragRunning: Boolean = true


    private fun getVolumeForSpecifiedDBLevel(hL : Int, frequency: Int): Float{

        // As Working with minuses for dbFS it needs to increase  conversion to increase volume
        val hL1 = -abs(hL)
        val spl = hL1 + conversionSPLtoHL[frequency]

        /*
         * Equation is:
         * Volume = Reference Volume * 10 ^ (db Gain / 20)
         * Where reference volume is 1 as that is max
         * and db Gain is based on dbFS so will need to do in minus.
         * E.g. 0 is 100% volume, -5 is -5dB of 100%
         */
        return 1 * 10.toDouble().pow(((spl/20))).toFloat()
    }
    // Calibration skipped
    fun runTest(doubleArray : DoubleArray){

        // When dbFS = 0 maximumSPLVolume is 96 as maximum possible with 16bit

        for (e in 0 until 2){
            if (isFragRunning){

                // Will start at 40dbHL which is -40 as it uses dbSPL
                var maxDB = doubleArray[0]

//            default db is 40 dbHL
                var currentDb = 40

                // Minuses 40 as the answer provides how much to reduce by to get to 40.
                var currentVolume = getVolumeForSpecifiedDBLevel((maxDB - 40).toInt(),0)

                for (x in testingFrequencies.indices){

                    if (isFragRunning){

                        maxDB = doubleArray[x]

                        while (!isBarelyAudible){
                            if (isFragRunning){

                                if (isResponseMade || isHeard == -1){
                                    //Means never played before
                                    if (isHeard == -1){
                                        currentVolume = getVolumeForSpecifiedDBLevel((maxDB - 40).toInt(),x)
                                        playSound(testingFrequencies[x].toFloat(),currentVolume)
                                        tvText.postValue(x.toString())
                                    }else if (isHeard == 0){
                                        // Cannot hear so increase volume
                                        isResponseMade = false

                                        // Last position in array so can't go any higher
                                        if (currentVolume > 1){
                                            toastMessage.postValue("Can't go any higher\nSwitched to next frequency")
                                            break
                                        }else{
                                            // Increase volume as not last position increases in 5dB
                                            currentDb += 5
                                            Log.d("ATMS",getVolumeForSpecifiedDBLevel((maxDB - currentDb).toInt(),x).toString())
                                            currentVolume = getVolumeForSpecifiedDBLevel((maxDB - currentDb).toInt(),x)
                                            playSound(testingFrequencies[x].toFloat(),currentVolume)
                                            tvText.postValue(x.toString())
                                        }
                                    }else if (isHeard == 1){

                                        isResponseMade = false

                                        // Cant go below 0 as no other volume left
                                        if (currentDb <= -10){
                                            toastMessage.postValue("Can't go any lower\nSwitched to next frequency")
                                            break
                                        }else {
                                            currentDb -= 10
                                            Log.d("ATMS",getVolumeForSpecifiedDBLevel((maxDB - currentDb).toInt(),x).toString())
                                            currentVolume = getVolumeForSpecifiedDBLevel((maxDB - currentDb).toInt(),x)
                                            playSound(testingFrequencies[x].toFloat(),currentVolume)
                                            tvText.postValue(x.toString())
                                        }
                                    }

                                }else{
                                    // Response made is already false so no need to declare again
                                    // No Button Pressed Repeat tone at same DB HL
                                    playSound(testingFrequencies[x].toFloat(),currentVolume)
                                    tvText.postValue(x.toString())
                                }
                                Thread.sleep(1000)
                            }else{
                                break
                            }
                        }

                        isBarelyAudible = false
                        // Set back to -1 so starts at 40dB for next frequency
                        isHeard = -1

//                Adds Entries

                        val ear : String = if(e == 0){
                            "Right"
                        }else{
                            "Left"
                        }

                        val entry = HearingResult(testingFrequencies[x],currentDb,ear,date)
                        hearingResultList.add(entry)

                        currentDb = 40
                        currentVolume = getVolumeForSpecifiedDBLevel((maxDB - currentDb).toInt(),x)

                        Thread.sleep(1000)
                    }else{
                        break
                    }
                }

                if (e == 0)
                    toastMessage.postValue("Swap to Left Ear")
                else
                    toastMessage.postValue("Calibration Complete \nView in Results")

            }else{
                break
            }
        }

        if (isFragRunning)
            isTestDone.postValue(true)

    }

    /**
     * Takes a [frequency] and initialises an instance with the
     * provided frequency.
     */
    private fun playSound(frequency: Float, volume :Float){

        toneThread?.stopThread()
        toneThread = ToneThread(frequency, volume, 1)
        toneThread?.start()

    }

    /**
     * Rather than calling the stop function on the Thread,
     * which is deprecated, instead the new method is to call
     * a function within the class that extends the thread and
     * modify a parameter which is used to check if it should still
     * run
     */
    fun stopSound() {
        isFragRunning = false
        toneThread?.stopThread()
        toneThread = null

    }


    fun saveTest(){

        doAsync {
            val testEntry = TestingResult(date,testType)
            testingResultRepository.insertTest(testEntry)
            hearingResultRepository.insertResultsForTest(hearingResultList)
        }
    }
}