package com.thedevelopera.project.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.thedevelopera.project.data.TestingResult
import com.thedevelopera.project.data.TestingResultRepository

class ResultFragmentViewModel internal constructor(testingResultRepository: TestingResultRepository): ViewModel(){

    val testResult: LiveData<List<TestingResult>> = testingResultRepository.getAllTests()

}
