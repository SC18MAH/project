package com.thedevelopera.project.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.thedevelopera.project.data.TestingResultRepository

class ResultFragmentViewModelFactory (
    private val repository: TestingResultRepository
): ViewModelProvider.NewInstanceFactory(){

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = ResultFragmentViewModel(
        repository
    ) as T
}