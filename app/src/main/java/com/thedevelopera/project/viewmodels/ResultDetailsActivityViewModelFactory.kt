package com.thedevelopera.project.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.thedevelopera.project.data.HearingResultRepository
import java.util.*

class ResultDetailsActivityViewModelFactory (
    private val hearingResultRepository: HearingResultRepository, private val date : Calendar
): ViewModelProvider.NewInstanceFactory(){

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = ResultDetailsActivityViewModel(
        hearingResultRepository,
        date
    ) as T
}