package com.thedevelopera.project.viewmodels

import android.app.Application
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.thedevelopera.project.utilities.ToneThread
import kotlin.math.*


class CalibrationViewModel (application: Application) :
    AndroidViewModel(application) {

    var calibratedArray : MutableLiveData<DoubleArray> = MutableLiveData()

    private val mSampleRate = 44100
    private val mBufferSize = AudioRecord.getMinBufferSize(mSampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT)
    //    Conversion found from :
//   https://hearinglosshelp.com/blog/understanding-the-difference-between-sound-pressure-level-spl-and-hearing-level-hl-in-measuring-hearing-loss/
    private val testingFrequencies = intArrayOf(1000, 2000, 4000, 6000, 8000,500,250)
    private val conversionSPLtoHL = doubleArrayOf(7.5, 9.0, 12.0, 16.0, 15.5, 13.5, 27.0)
    private val mCalibration = DoubleArray(7)
    private var inputSignalImaginary = DoubleArray(2048)
    private var toneThread : ToneThread? = null
    private var isFragRunning: Boolean = true

    // The following three methods are taken from https://github.com/ReeceStevens/ut_ewh_audiometer_2014/blob/master/app/src/main/java/ut/ewh/audiometrytest/Calibration.java
    private fun newBitReverse(j: Int): Int {

        var oldBit = j
        var newBit = 0
        while (oldBit != 0) {
            newBit = newBit shl 1
            newBit = newBit or (oldBit and 1)
            oldBit = oldBit shr 1
        }
        return newBit
    }

    /**
     * This method using the FFT allows to view the signal in a different domain, in this case
     * a frequency domain. This method is especially useful as it can determine what frequencies
     * are present in the signal.
     *
     * @param realNumberArr contains all the real numbers, these are +ve numbers
     * @param imaginaryNumberArr contains all the imaginary numbers, these are -ve numbers
     */
    private fun fftAnalysis(realNumberArr: DoubleArray, imaginaryNumberArr: DoubleArray): DoubleArray {
        val samples = 2048
        val nu = 11
        val bufferReal = DoubleArray(samples)
        val shortenedReal = DoubleArray(samples)
        //shorten buffer data to a power of two
        for (s in 0 until samples) {
            shortenedReal[s] = realNumberArr[s]
        }
        // Computing the coefficients for everything ahead of time
        val real = Array(nu + 1) { DoubleArray(samples) }
        var counter = 2
        for (l in 1..nu) {
            for (i in 0 until samples) {
                real[l][i] = cos((2.toDouble()) * Math.PI * (i.toDouble()) / (counter.toDouble()))
            }
            counter *= 2
        }
        val imaginary = Array(nu + 1) { DoubleArray(samples) }
        counter = 2
        for (l in 1..nu) {
            for (i in 0 until samples) {
                imaginary[l][i] = -1 * sin((2.toDouble()) * Math.PI * (i.toDouble()) / (counter.toDouble()))
            }
            counter *= 2
        }

        // Populate bufferReal with inputReal in bit-reversed order
        for (x in shortenedReal.indices) {
            val p = newBitReverse(x)
            bufferReal[x] = shortenedReal[p]
        }
        // begin transform
        var step = 1
        for (level in 1..nu) {
            val increment = step * 2
            for (j in 0 until step) {
                var i = j
                while (i < samples) {
                    var realCoefficient = real[level][j]
                    var imagCoefficient = imaginary[level][j]
                    realCoefficient *= bufferReal[i + step]
                    imagCoefficient *= bufferReal[i + step]
                    bufferReal[i + step] = bufferReal[i]
                    imaginaryNumberArr[i + step] = imaginaryNumberArr[i]
                    bufferReal[i + step] -= realCoefficient
                    imaginaryNumberArr[i + step] -= imagCoefficient
                    bufferReal[i] += realCoefficient
                    imaginaryNumberArr[i] += imagCoefficient
                    i += increment
                }
            }
            step *= 2
        }
        val transformResult = DoubleArray(bufferReal.size)
        // Calculate magnitude of FFT coefficients
        for (q in bufferReal.indices) {
            transformResult[q] = sqrt(bufferReal[q].pow(2.0) + imaginaryNumberArr[q].pow(2.0))
        }
        return transformResult

    }

    private fun dbListen(frequency:Int):DoubleArray {
        val rmsArray = DoubleArray(5)
        for (j in rmsArray.indices)
        {
            val audioRecord = AudioRecord(MediaRecorder.AudioSource.MIC, mSampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, mBufferSize)
            val buffer = ShortArray(mBufferSize)
            try
            {
                audioRecord.startRecording()
            }
            catch (e:IllegalStateException) {
                Log.e("Error", "Microphone permission not given")
            }

            // This  is required to ensure that the results are written into the buffer
            audioRecord.read(buffer, 0, buffer.size)

            //Convert buffer from type short[] to double[]
            val inputSignal = DoubleArray(buffer.size)
            for (x in buffer.indices)
            {
                inputSignal[x] = buffer[x].toDouble()
            }
            val outputSignal = fftAnalysis(inputSignal, inputSignalImaginary)
            val k = frequency * 2048 / mSampleRate // Selects the value from the transform array corresponding to the desired frequency
            rmsArray[j] = outputSignal[k]
            audioRecord.stop()
            audioRecord.release()
        }
        return rmsArray
    }

    fun runTest(){

        for (i in testingFrequencies.indices){

            // Will stop thread if fragment is exited
            if (isFragRunning){
                var dbAverage = 0.00
                while (dbAverage < 40 || dbAverage > 120){
                    // Will stop thread if fragment is exited
                    if (isFragRunning){
                        val frequency = testingFrequencies[i]
                        toneThread =
                            ToneThread(
                                frequency.toFloat(),
                                30000f,
                                4
                            )

                        // The backgroundRMS is the output voltage before playing the tone
                        val backgroundRms = dbListen(frequency)

                        toneThread?.start()

                        // The soundRMS is the output voltage after playing the tone
                        val soundRMS = dbListen(frequency)

                        val resultingRMS = DoubleArray(5)
                        val resultingDB = DoubleArray(5)

                        for (x in resultingRMS.indices){

                            // Provides the RMS difference of the Tone relative to the background
                            resultingRMS[x] = backgroundRms[x] / soundRMS[x]
                            // This provides the db SPL version
                            resultingDB[x] = 20 * log10(resultingRMS[x]) + 96
                        }

                        for (element in resultingDB){
                            dbAverage += element
                        }

                        // Provides the average DB HL from the 5 samples taken
                        dbAverage /= (resultingDB.size)

                        mCalibration[i] = dbAverage
                        toneThread?.stopThread()
                    }else{
                        break
                    }
                }
            }else{
                break
            }

        }
        // Ensures data is not saved if fragment is destroyed
        if (isFragRunning)
            saveData(mCalibration)

    }

    // This method does not save the data instead it returns it to observer
    // So it can use it thus reducing read and write times
    private fun saveData(doubleArray: DoubleArray){

        calibratedArray.postValue(doubleArray)
    }

    //Convert to HL from SPL and post value
    fun calibrationFromAudiometerIsComplete(array: DoubleArray){

        val dbHLResultsArray = DoubleArray(7)

        //Converts dB SPL values to dBHL
        for (i in testingFrequencies.indices){
            val dbHL = array[i] - conversionSPLtoHL[i]
            dbHLResultsArray[0] = dbHL
        }

        saveData(dbHLResultsArray)

    }

    fun playTone(counter:Int){

        toneThread?.stopThread()
        toneThread = ToneThread(
            testingFrequencies[counter].toFloat(),
            30000.toFloat(),
            120
        )
        toneThread?.start()

    }

    fun stopTone(){
        //Used by MicCalibrationFragment to stop thread after exiting
        isFragRunning = false
        toneThread?.stopThread()
    }

}
