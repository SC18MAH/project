package com.thedevelopera.project.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.thedevelopera.project.data.HearingResultRepository
import com.thedevelopera.project.data.TestingResultRepository

class PureToneTestViewModelFactory (
    private val testingResultRepository: TestingResultRepository,
    private val hearingResultRepository: HearingResultRepository

): ViewModelProvider.NewInstanceFactory(){

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        PureToneTestViewModel(
            testingResultRepository,
            hearingResultRepository
        ) as T
}