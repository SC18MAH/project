package com.thedevelopera.project.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.thedevelopera.project.data.HearingResult
import com.thedevelopera.project.data.HearingResultRepository
import java.util.*

class ResultDetailsActivityViewModel internal constructor(hearingResultRepository: HearingResultRepository
, date : Calendar): ViewModel(){


    // Will fetch data from db to be Used in UI
    val hearingResult: LiveData<List<HearingResult>> = hearingResultRepository.getResultsForTest(date)

}