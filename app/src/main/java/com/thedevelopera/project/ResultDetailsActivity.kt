package com.thedevelopera.project

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.thedevelopera.project.data.HearingResult
import com.thedevelopera.project.utilities.InjectRepos
import com.thedevelopera.project.viewmodels.ResultDetailsActivityViewModel
import kotlinx.android.synthetic.main.activity_result_details.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ResultDetailsActivity : AppCompatActivity() {

    private var viewModel : ResultDetailsActivityViewModel? = null
    private var results: List<HearingResult>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_details)

        // Fetches argument
        // passed by TestFragment when RV item clicked
        // Can't pass Calendar object directly so must convert
        val timeInMilli = intent.getLongExtra("time", -1)
        val timeZoneId = intent.getStringExtra("timezone")

        val calendar = GregorianCalendar(TimeZone.getTimeZone(timeZoneId))
        calendar.timeInMillis = timeInMilli


        val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())

        Log.d("ATMS claendar",formatter.format(calendar.time))

        viewModel = ViewModelProviders.of(this,InjectRepos.provideResultDetailsActivityViewModelFactory(this,calendar))
            .get(ResultDetailsActivityViewModel::class.java)


        viewModel?.hearingResult?.observe(this, Observer {
            t ->

            results = t

            displayChart()
        })
    }

    private fun displayChart(){

        val rightListEntries = ArrayList<Entry>(7)
        val leftListEntries = ArrayList<Entry>(7)

        // New Left List and Right List as frequency 250 & 500
//        Appear at end of list but cannot show on chart , so must re order
        val newLeftResults = ArrayList<HearingResult>()
        val newRightResults = ArrayList<HearingResult>()

        // Adds 250 Frequency to 1 st Position
        newRightResults.add(results!![6])
        newLeftResults.add(results!![13])
//        Adds 500 Frequency to 2nd Position
        newRightResults.add(results!![5])
        newLeftResults.add(results!![12])

        //Adds the rest after the the first 2
        for (i in 0..4){
            newRightResults.add(results!![i])
        }

        for (i in 7..11 ){
            newLeftResults.add(results!![i])
        }

        for (i in 0 until 7){
            leftListEntries.add(Entry(newLeftResults[i].hearingFrequency.toFloat(),newLeftResults[i].hearingHL.toFloat()))
            rightListEntries.add(Entry(newRightResults[i].hearingFrequency.toFloat(),newRightResults[i].hearingHL.toFloat()))
        }

        val leftLineDataSet = LineDataSet(leftListEntries,"Left Ear")
        val rightLineDataSet = LineDataSet(rightListEntries,"Right Ear")

        leftLineDataSet.lineWidth = 1f
        rightLineDataSet.lineWidth = 1f

        leftLineDataSet.color = Color.BLUE
        leftLineDataSet.setCircleColor(Color.BLUE)
        leftLineDataSet.circleRadius = 6f
        leftLineDataSet.circleHoleRadius = 3f
//        Hides values from displaying
        leftLineDataSet.setDrawValues(false)

        rightLineDataSet.color = Color.RED
        rightLineDataSet.setCircleColor(Color.RED)
        rightLineDataSet.circleRadius = 6f
        rightLineDataSet.circleHoleRadius = 3f
        rightLineDataSet.setDrawValues(false)

        val lineData = LineData(leftLineDataSet,rightLineDataSet)

        line_chart_result_details_activity.data = lineData

        line_chart_result_details_activity.xAxis.position = XAxis.XAxisPosition.TOP


        val xValsDateLabel = ArrayList<String>()
        xValsDateLabel.add("250")
        xValsDateLabel.add("500")
        xValsDateLabel.add("1000")
        xValsDateLabel.add("2000")
        xValsDateLabel.add("4000")
        xValsDateLabel.add("6000")
        xValsDateLabel.add("8000")


        line_chart_result_details_activity.xAxis.valueFormatter = (MyValueFormatter(xValsDateLabel))

        // By default both sides are enabled so removing on right side
        line_chart_result_details_activity.axisRight.isEnabled = false
        line_chart_result_details_activity.description.text = "DB HL Hearing Levels"
        line_chart_result_details_activity.description.textSize = 12f
        line_chart_result_details_activity.axisLeft.isInverted = true
        line_chart_result_details_activity.axisLeft.axisMinimum = -10f
        line_chart_result_details_activity.axisLeft.axisMaximum = 100f
        line_chart_result_details_activity.axisLeft.labelCount = 12

        line_chart_result_details_activity.axisLeft.setDrawLimitLinesBehindData(true)
        val normalLimitLine = LimitLine(20f)
        normalLimitLine.lineColor = Color.BLUE
        normalLimitLine.lineWidth = 5f
        normalLimitLine.enableDashedLine(20f,8f,0f)
        line_chart_result_details_activity.axisLeft.addLimitLine(normalLimitLine)

        val mildLimitLine = LimitLine(40f)
        mildLimitLine.lineColor = Color.GREEN
        mildLimitLine.lineWidth = 4f
        mildLimitLine.enableDashedLine(20f,8f,0f)
        line_chart_result_details_activity.axisLeft.addLimitLine(mildLimitLine)

        val moderateLimitLine = LimitLine(70f)
        moderateLimitLine.lineColor = Color.YELLOW
        moderateLimitLine.lineWidth = 3f
        moderateLimitLine.enableDashedLine(20f,8f,0f)
        line_chart_result_details_activity.axisLeft.addLimitLine(moderateLimitLine)


        val severeLimitLine = LimitLine(100f)
        severeLimitLine.lineColor = Color.RED
        severeLimitLine.lineWidth = 2f
        severeLimitLine.enableDashedLine(20f,8f,0f)
        line_chart_result_details_activity.axisLeft.addLimitLine(severeLimitLine)
    }


    class MyValueFormatter(private val xValsDateLabel: ArrayList<String>) : ValueFormatter() {

        override fun getFormattedValue(value: Float): String {

            return value.toString()
        }

        override fun getAxisLabel(value: Float, axis: AxisBase): String {
            when (value.toInt()) {

                250 -> return xValsDateLabel[0]

                500 -> return xValsDateLabel[1]

                1000 -> return xValsDateLabel[2]

                2000 -> return xValsDateLabel[3]

                4000 -> return xValsDateLabel[4]

                6000 -> return xValsDateLabel[5]

                8000 -> return xValsDateLabel[6]
            }

            return ""

        }
    }

}

