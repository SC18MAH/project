package com.thedevelopera.project

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.thedevelopera.project.utilities.InjectRepos
import com.thedevelopera.project.viewmodels.PureToneTestViewModel
import kotlinx.android.synthetic.main.activity_pure_tone_test.*
import org.jetbrains.anko.doAsync


class PureToneTestActivity : AppCompatActivity() {

    private var viewModel: PureToneTestViewModel? = null
    private var isBarelyAudible = false
    //    Int used instead of Boolean as cannot make it null, so using int, -1 = Null, 0 = No, 1 = Yes
    private var isHeard: Int = -1
    private var isResponseMade = false
    // These results were provided using the stock earphones provided with the Samsung S10
    // Following format used: 1000, 2000, 4000, 6000, 8000, 500, 250
    private val defaultMaxForFrequenciesArray = doubleArrayOf(85.0,92.0,104.0,90.0,80.0,89.0,95.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pure_tone_test)

        viewModel = ViewModelProviders.of(this, InjectRepos.providePureToneTestActivityViewModelFactory(this)).get(
            PureToneTestViewModel::class.java)

        // Fetches argument passed by PureToneActivity
        val data = intent.getDoubleArrayExtra("array")

        if (data != null){

            // Calibration was done so use those values
            doAsync { viewModel?.runTest(data) }

        }else{

            // Calibration was skipped so use default values
            doAsync { viewModel?.runTest(defaultMaxForFrequenciesArray) }

        }

        viewModel?.tvText?.observe(this, Observer {
            it?.let {
                tv_pure_tone_test_activity.text = getString(R.string.string_frequencies_done,it.toInt())
            }
        })

        viewModel?.toastMessage?.observe(this, Observer {

            it?.let{

                Toast.makeText(this,it,Toast.LENGTH_LONG).show()
            }
        })

        viewModel?.isTestDone?.observe(this, Observer {

            it?.let {

                if (it){
                    viewModel?.saveTest()
                    setResult(Activity.RESULT_OK)
                    finish()
                }

            }
        })

        can_btn_pure_tone_test_activity.setOnClickListener {

            runOnUiThread{
                view_pure_tone_test_activity.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.colorGreen))
            }

            viewModel?.isHeard = 1
            viewModel?.isResponseMade = true

            isHeard = 1
            isResponseMade = true

        }

        cant_btn_pure_tone_test_activity.setOnClickListener{

            runOnUiThread{
                view_pure_tone_test_activity.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.colorRed))
            }

            viewModel?.isHeard = 0
            viewModel?.isResponseMade = true

            isHeard = 0
            isResponseMade = true
        }

        btn_barely_audible_pure_tone_activity.setOnClickListener {

            runOnUiThread{
                view_pure_tone_test_activity.setBackgroundColor(ContextCompat.getColor(applicationContext,R.color.colorWhite))
            }

            viewModel?.isBarelyAudible = true
            isBarelyAudible = true
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        viewModel?.stopSound()
        setResult(Activity.RESULT_CANCELED)
        finish()

    }


}