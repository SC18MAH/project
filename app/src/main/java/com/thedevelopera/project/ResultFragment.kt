package com.thedevelopera.project


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.thedevelopera.project.adapters.ResultAdapter
import com.thedevelopera.project.data.TestingResult
import com.thedevelopera.project.utilities.InjectRepos
import com.thedevelopera.project.utilities.RecyclerTouchListener
import com.thedevelopera.project.viewmodels.MainViewModel
import com.thedevelopera.project.viewmodels.ResultFragmentViewModel
import kotlinx.android.synthetic.main.fragment_result.*

/**
 * Fragment which displays results from tests to the user.
 */
class ResultFragment : Fragment() {

    private var viewModel : ResultFragmentViewModel? = null

    private var parentViewModel: MainViewModel? = null

    var results: List<TestingResult> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    companion object {
        fun newInstance(): ResultFragment = ResultFragment()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            parentViewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }

        viewModel = ViewModelProviders.of(this,
            InjectRepos.provideResultFragmentViewModelFactory(this.requireContext()))
            .get(ResultFragmentViewModel::class.java)


        viewModel?.testResult?.observe(this, Observer {
            t ->

            results = t
            rv_result_fragment.adapter =
                ResultAdapter(results, this.requireContext())
        })


        rv_result_fragment.layoutManager = LinearLayoutManager(this.context)
        rv_result_fragment.addItemDecoration(DividerItemDecoration(this.requireContext(), DividerItemDecoration.VERTICAL))
        rv_result_fragment.addOnItemTouchListener(

            RecyclerTouchListener(
                this.requireContext(),
                rv_result_fragment,

                object : RecyclerTouchListener.ClickListener {
                    override fun onLongClick(view: View?, position: Int) {
                    }

                    override fun onClick(view: View, position: Int) {

/*                        Position represents the fragment to change to.
                          Currently only 1 method but in the future possible
                          that there could be more
 */
                        val time = results[position].testDate.timeInMillis
                        val timeZone = results[position].testDate.timeZone

                        parentViewModel?.argsToPass?.set(0, time.toString())
                        parentViewModel?.argsToPass?.set(1, timeZone.toString())
                        parentViewModel?.inputNumber?.postValue(1)


                    }
                })
            )

    }
}
