package com.thedevelopera.project


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.thedevelopera.project.viewmodels.CalibrationViewModel
import kotlinx.android.synthetic.main.fragment_mic_calibration.*
import org.jetbrains.anko.doAsync


/**
 * This fragment is responsible for provide the db SPL using the inbuilt Mic from the phone
 */
class MicCalibrationFragment : Fragment() {

    private var viewModel : CalibrationViewModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mic_calibration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(CalibrationViewModel::class.java)
        }


        btn_mic_calibration_fragment.setOnClickListener {
            runCalibration()
        }


    }

    private fun runCalibration(){

        doAsync {

//            Will run on background task with the help of Anko Library
            viewModel?.runTest()


        }
    }

    companion object{
        fun newInstance(): MicCalibrationFragment = MicCalibrationFragment()
    }
}
