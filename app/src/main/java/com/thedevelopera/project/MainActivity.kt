package com.thedevelopera.project

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.thedevelopera.project.viewmodels.MainViewModel


/**
 * This is the launcher class within the application
 * and is responsible for handling the fragments displayed
 * from the [BottomNavigationView]
 */
class MainActivity : AppCompatActivity() {

    private var mainViewModel : MainViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigation_main)

        //Default item in the navigation bar is selected and launched
        bottomNavigation.selectedItemId = R.id.menu_item_tests
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val testFragment = TestFragment.newInstance()
        swapFragment(testFragment)

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        mainViewModel?.inputNumber?.observe(this, Observer {it ->
            it?.let { changeActivity(it) }
        })

        if (!checkPermission())
            requestPermission()
    }


    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {

        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO),
            1
        )
    }


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.menu_item_tests -> {
                val testFragment = TestFragment.newInstance()
                swapFragment(testFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.menu_item_results -> {
                val resultFragment = ResultFragment.newInstance()
                swapFragment(resultFragment)
                return@OnNavigationItemSelectedListener true
            }

            R.id.menu_item_advice -> {
                val adviceFragment = AdviceFragment.newInstance()
                swapFragment(adviceFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * A reusable method to handle the fragment transactions
     * when needing to swap fragments. [fragment] provides the
     * fragment that it should swap to
     */
    private fun swapFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container_main, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    /**
     * Changes activity, commonly used from fragment when needing to go to another
     * activity. [int] is a number associated to each activity which helps to identify
     * which to send to.
     */
    private fun changeActivity(int: Int){

        val intent:Intent
        when (int){

            0 -> {
                intent = Intent(this, PureToneActivity::class.java)
                startActivity(intent)
            }

            // Refers to sending data from ResultFragment to ResultDetailsActivity
            1 -> {

                val timeInMilli = mainViewModel!!.argsToPass[0]?.toLong()
                val timeZone = mainViewModel!!.argsToPass[1]

                intent = Intent(this, ResultDetailsActivity::class.java)
                intent.putExtra("time",timeInMilli)
                intent.putExtra("timezone", timeZone)
                startActivity(intent)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        // Requires permission for microphone
        if (requestCode == 1){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this,"Permission not granted\nSome features will not work",Toast.LENGTH_LONG).show()
            }
        }
    }
}