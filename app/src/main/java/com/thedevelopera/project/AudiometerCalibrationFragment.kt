package com.thedevelopera.project


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.thedevelopera.project.viewmodels.CalibrationViewModel
import kotlinx.android.synthetic.main.fragment_audiometer_calibration.*
import org.jetbrains.anko.doAsync

class AudiometerCalibrationFragment : Fragment() {

    private var parentViewModel: CalibrationViewModel? = null
    private val mCalibration = DoubleArray(7)
    private var counter = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_audiometer_calibration, container, false)
    }

    companion object{
        fun newInstance(): AudiometerCalibrationFragment = AudiometerCalibrationFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            parentViewModel = ViewModelProviders.of(it).get(CalibrationViewModel::class.java)
        }

        btn_audiometer_calibration.setOnClickListener {

            btnClicked()
        }
    }

    private fun btnClicked(){

        // Depending on current text, operation will be notified
        when (btn_audiometer_calibration.text){

            "START" -> { startPressed()}

            "SUBMIT" -> { submitPressed()}
        }
    }

    private fun submitPressed(){

        if (counter >= 7){

            parentViewModel?.stopTone()
            Toast.makeText(this.requireContext(),"Calibration Complete",Toast.LENGTH_SHORT).show()

        }else if ( counter < 6){

            if (et_audiometer_calibration.text.toString() != ""){
                // Adds value to EditText entered in the mCalibration array
                mCalibration[counter] = (et_audiometer_calibration.text.toString().toDouble())

                //Clears after submitting
                et_audiometer_calibration.setText("")
                counter++

                doAsync {
                    parentViewModel?.playTone(counter)
                }


            }

        }else if (counter == 6){
            // Code runs when last entry is added and not empty
            if (et_audiometer_calibration.text.toString() != ""){

                parentViewModel?.stopTone()
                Toast.makeText(this.requireContext(),"Calibration Complete",Toast.LENGTH_SHORT).show()

                mCalibration[counter] = (et_audiometer_calibration.text.toString().toDouble())

                parentViewModel?.calibrationFromAudiometerIsComplete(mCalibration)
                counter++


            }


        }

    }

    /*
     * This method will begin playing tone once the start button is pressed,
     * will also change the text of button too.
     */
    private fun startPressed(){

        btn_audiometer_calibration.text = getString(R.string.string_submit)

        et_audiometer_calibration.visibility = View.VISIBLE

        doAsync {

            parentViewModel?.playTone(counter)

        }
    }


}
