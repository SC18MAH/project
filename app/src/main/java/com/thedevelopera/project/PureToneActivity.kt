package com.thedevelopera.project

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/**
 * This Activity is responsible for displaying the calibration methods that are available
 */
class PureToneActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pure_tone)

        launchDialog()

    }

    // Provides an AlertDialog which provides available calibration options
    private fun launchDialog(){

        val calibrationMethods = arrayOf("Calibrate with Audiometer",
            "Calibrate using in-built MIC (Beta)",
            "Skip Calibration")

        val builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)

        builder.setTitle("Choose Calibration Method")


        builder.setItems(calibrationMethods) { _, which ->

            if (which != 2){
                // Which refers to the calibration method selected
                calibrationRequired(which)
            }else{
                skippedCalibration()
            }


        }

        builder.setOnCancelListener {

            Toast.makeText(this,"Calibration Cancelled", Toast.LENGTH_SHORT).show()
            onBackPressed()
        }
        val dialog = builder.create()

        dialog.show()
    }


    // Populate default values. 86.4dB maximum input at 90%
    private fun skippedCalibration(){
        intent = Intent(this, PureToneTestActivity::class.java)
        startActivityForResult(intent,1)
    }


    /**
     * Changes activity, commonly used from fragment when needing to go to another
     * activity.
     */
    private fun calibrationRequired(calibrationMethod : Int){

        val intent = Intent(this,CalibrationActivity::class.java)

        intent.putExtra("calibration", calibrationMethod)

        startActivityForResult(intent,0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

//        Calibration complete
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 1){
                finish()
            }else{

                val intent = Intent(this,PureToneTestActivity::class.java)

                intent.putExtra("Array", data?.getDoubleArrayExtra("Array"))

                startActivity(intent)
            }


        }else if (resultCode == Activity.RESULT_CANCELED){

            Toast.makeText(this,"Calibration Cancelled", Toast.LENGTH_SHORT).show()
            onBackPressed()
        }

    }
}
