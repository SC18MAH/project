package com.thedevelopera.project

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.thedevelopera.project.viewmodels.CalibrationViewModel

class CalibrationActivity : AppCompatActivity() {


    private var viewModel: CalibrationViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calibration)

        // Fetches argument passed by PureToneActivity
        val param = intent.getIntExtra("calibration",-1)

        var fragment : Fragment? = null

        volumeChange()

        // Changes volume on phone
        when (param) {
            0 -> {
                // Calibrate with Audiometer
                fragment = AudiometerCalibrationFragment.newInstance()
            }
            1 -> {
                // Calibrate using Mic
                fragment = MicCalibrationFragment.newInstance()
            }
        }

        swapFragment(fragment!!)

        viewModel =  ViewModelProviders.of(this).get(CalibrationViewModel::class.java)

        viewModel?.calibratedArray?.observe(this , Observer {

            it?.let {

                finishedCalibration()
            }
        })

    }

    private fun finishedCalibration(){

        val intent = Intent(this,PureToneActivity::class.java)
        intent.putExtra("Array",viewModel?.calibratedArray?.value)
        setResult(Activity.RESULT_OK,intent)
        finish()

    }

    // Sets volume to max by default to allow tones to play at loudest
    private fun volumeChange(){
        val audioManager : AudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        val maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,maxVolume,0)
    }

    /**
     * A reusable method to handle the fragment transactions
     * when needing to swap fragments. [fragment] provides the
     * fragment that it should swap to
     */
    private fun swapFragment(fragment: Fragment){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container_calibration, fragment)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        // Used to stop tone when back button is pressed
        viewModel?.stopTone()
        setResult(Activity.RESULT_CANCELED)
        finish()

    }
}
