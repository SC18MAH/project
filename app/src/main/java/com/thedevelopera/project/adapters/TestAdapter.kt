package com.thedevelopera.project.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thedevelopera.project.R
import kotlinx.android.synthetic.main.layout_test_list_item.view.*

class TestAdapter (private val items: ArrayList<String>, val context: Context): RecyclerView.Adapter<TestAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTestName?.text = items[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_test_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){

        val tvTestName: TextView? = view.tv_layout_list_item
    }
}