package com.thedevelopera.project.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thedevelopera.project.R
import com.thedevelopera.project.data.TestingResult
import kotlinx.android.synthetic.main.layout_result_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class ResultAdapter (private val items: List<TestingResult>, val context: Context): RecyclerView.Adapter<ResultAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle?.text = items[position].testType
        val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())

        holder.tvDate?.text = formatter.format(items[position].testDate.time)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.layout_result_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){

        val tvTitle: TextView? = view.tv_layout_result_item_title
        val tvDate: TextView? = view.tv_layout_result_item_date
    }
}