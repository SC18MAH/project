package com.thedevelopera.project


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.thedevelopera.project.adapters.TestAdapter
import com.thedevelopera.project.utilities.RecyclerTouchListener
import com.thedevelopera.project.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.fragment_test.*
import com.thedevelopera.project.utilities.RecyclerTouchListener.ClickListener as ClickListener1

/**
 * Fragment which displays all the possible tests available
 */
class TestFragment : Fragment() {

    private var viewModel: MainViewModel? = null

    // Contains all different types of testing methods
    private val tests: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    companion object{
        fun newInstance(): TestFragment = TestFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }

        // As currently doesn't get from Database its pre-filled
        // This check is used when pressing back button to ensure RV doesn't duplicate item
        if (tests.size != 1){
            tests.add("Pure Tone Audiometry")
        }

        rv_test_fragment.layoutManager = LinearLayoutManager(this.context)

        rv_test_fragment.adapter =
            TestAdapter(tests, this.requireContext())
        rv_test_fragment.addItemDecoration(DividerItemDecoration(this.requireContext(),DividerItemDecoration.VERTICAL))

        rv_test_fragment.addOnItemTouchListener(
            RecyclerTouchListener(
                this.requireContext(),
                rv_test_fragment,
                object : ClickListener1 {
                    override fun onLongClick(view: View?, position: Int) {

                    }

                    override fun onClick(view: View, position: Int) {

/*                        Position represents the fragment to change to.
                          Currently only 1 method but in the future possible
                          that there could be more
 */
                        viewModel?.inputNumber?.postValue(position)

                    }
                })
        )

    }
}
