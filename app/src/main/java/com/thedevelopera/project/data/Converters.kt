
/*
 * Code taken from: https://github.com/googlesamples/android-sunflower/blob/master/app/src/main/java/com/google/samples/apps/sunflower/data/Converters.kt
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thedevelopera.project.data

import androidx.room.TypeConverter
import java.util.Calendar

/**
 * Type converters to allow Room to reference complex data types. In this case helps with Calendar type
 */
class Converters {
    @TypeConverter fun calendarToDatetime(calendar: Calendar): Long = calendar.timeInMillis

    @TypeConverter fun datetimeToCalendar(value: Long): Calendar =
        Calendar.getInstance().apply { timeInMillis = value }
}

