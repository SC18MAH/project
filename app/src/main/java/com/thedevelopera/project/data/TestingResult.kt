package com.thedevelopera.project.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


/**
 * [TestingResult] represents the results from a test undertaken. Again this will list all
 * the Tests not the actual results which will be from [HearingResult], it saves the id from
 * here.
 */
@Entity(
    tableName = "testing_result"
)
data class TestingResult(

    /**
     * Shows when the test was created
     */
    @ColumnInfo(name = "testing_results_date") var testDate: Calendar = Calendar.getInstance(),

    /**
     * Shows what type of test is being
     */
    @ColumnInfo(name = "testing_results_type") var testType : String
){

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "testing_results_id")
    var testId: Int = 0
}