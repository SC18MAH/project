package com.thedevelopera.project.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TestingResultDao{

    @Query("SELECT * FROM testing_result ORDER BY testing_results_date DESC")
    fun getAllTests(): LiveData<List<TestingResult>>

    @Insert
    fun insertNewTest(testingResult: TestingResult)
}