package com.thedevelopera.project.data

class TestingResultRepository private constructor(private val testingResultDao: TestingResultDao){

    fun getAllTests() = testingResultDao.getAllTests()

    fun insertTest(testingResult: TestingResult) = testingResultDao.insertNewTest(testingResult)

    //    Singleton Instantiation
    companion object{

        @Volatile private var instance: TestingResultRepository? = null

        fun getInstance(testingResultDao: TestingResultDao) =
            instance ?: synchronized(this){
                instance ?: TestingResultRepository(testingResultDao).also { instance = it }
            }
    }

}