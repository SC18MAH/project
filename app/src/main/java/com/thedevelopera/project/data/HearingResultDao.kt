package com.thedevelopera.project.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import java.util.*

/**
 * Data Access Object for [HearingResult]
 */
@Dao
interface HearingResultDao {

    @Query("SELECT * FROM hearing_result WHERE hearing_results_parent_date = :date")
    fun getHearingResultsForTest(date: Calendar): LiveData<List<HearingResult>>

    @Insert
    fun insertHearingResultsForTest(hearingResult: HearingResult)
}