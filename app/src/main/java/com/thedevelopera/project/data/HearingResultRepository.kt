package com.thedevelopera.project.data

import java.util.*

class HearingResultRepository private constructor(private val hearingResultDao: HearingResultDao){

    fun getResultsForTest(date: Calendar) = hearingResultDao.getHearingResultsForTest(date)

    fun insertResultsForTest(hearingResultList: List<HearingResult>){

        for (i in hearingResultList.indices){
            hearingResultDao.insertHearingResultsForTest(hearingResultList[i])
        }
    }


//    Singleton Instantiation
    companion object{

        @Volatile private var instance: HearingResultRepository? = null

        fun getInstance(hearingResultDao: HearingResultDao) =
            instance ?: synchronized(this){
                instance ?: HearingResultRepository(hearingResultDao).also { instance = it }
            }
    }

}

