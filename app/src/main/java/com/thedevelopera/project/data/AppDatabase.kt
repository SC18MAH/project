package com.thedevelopera.project.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

// This implementation was based of the Android Sunflower library available at : https://github.com/googlesamples/android-sunflower
@Database(entities = [TestingResult::class, HearingResult::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase(){

    abstract fun hearingResultDao(): HearingResultDao
    abstract fun testingResultDao(): TestingResultDao

    companion object {

        // Used to ensure only one instance of AppDatabase is available
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it }
        }


        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "project.db").build()
    }
}

