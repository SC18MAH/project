package com.thedevelopera.project.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    tableName = "hearing_result"
)
data class HearingResult(

    @ColumnInfo(name = "hearing_results_frequency") var hearingFrequency : Int,

    @ColumnInfo(name = "hearing_results_hl") var hearingHL : Int,

    @ColumnInfo(name = "hearing_results_ear") var hearingEar : String,

    @ColumnInfo(name = "hearing_results_parent_date") var parentDate: Calendar = Calendar.getInstance()
    ){

    // Primary key will increment automatically depending on existing values
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "hearing_results_id")
    var testId: Int = 0

}