package com.thedevelopera.project.utilities

import android.content.Context
import com.thedevelopera.project.viewmodels.PureToneTestViewModelFactory
import com.thedevelopera.project.viewmodels.ResultDetailsActivityViewModelFactory
import com.thedevelopera.project.viewmodels.ResultFragmentViewModelFactory
import com.thedevelopera.project.data.AppDatabase
import com.thedevelopera.project.data.HearingResultRepository
import com.thedevelopera.project.data.TestingResultRepository
import java.util.*

/**
 * Used to inject repos to ViewModel as a ViewModelFactory is required to
 * be able to pass variables using constructor
 */

object InjectRepos {

    private fun getTestingResultRepository(context: Context): TestingResultRepository{
        return TestingResultRepository.getInstance(
            AppDatabase.invoke(context).testingResultDao()
        )
    }

    private fun getHearingResultRepository(context: Context): HearingResultRepository{
        return HearingResultRepository.getInstance(
            AppDatabase.invoke(context).hearingResultDao()
        )
    }

    fun provideResultFragmentViewModelFactory(
        context: Context
    ): ResultFragmentViewModelFactory {
        return ResultFragmentViewModelFactory(
            getTestingResultRepository(context)
        )
    }

    fun provideResultDetailsActivityViewModelFactory(
        context: Context,
        date: Calendar
    ): ResultDetailsActivityViewModelFactory {
        return ResultDetailsActivityViewModelFactory(
            getHearingResultRepository(context),
            date
        )
    }

    fun providePureToneTestActivityViewModelFactory(
        context: Context
    ): PureToneTestViewModelFactory {
        return PureToneTestViewModelFactory(
            getTestingResultRepository(context),
            getHearingResultRepository(context)
        )
    }

}