package com.thedevelopera.project.utilities;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import org.jetbrains.annotations.NotNull;

public class ToneThread extends Thread{
    private final int sampleRate = 44100;
    private final float volume;
    private boolean isComplete = false;
    private final byte[] mGeneratedTone;
    private AudioTrack mAudioTrack;

    public ToneThread(float frequencyOfTone, float volume,int duration){
        mGeneratedTone =  genTone(frequencyOfTone,duration);
        this.volume = volume;
    }
    public void stopThread() {
        isComplete = false;
        releaseAudioTrack();
    }

    /**
     *  Stops the [mAudioTrack] instance, responsible for playing
     *  the sound and setting it to null after releasing the resources
     */
    private void releaseAudioTrack(){
        if (mAudioTrack!= null){
            mAudioTrack.stop();
            mAudioTrack.flush();
            mAudioTrack.release();
            mAudioTrack = null;
        }
    }

    /**
     * Generates the tone based on the increment and volume, used in inner loop
     *
     */
    private byte[] genTone(float frequencyOfTone, int duration) {

        /*
         * increment refers to the growth of the sine wave and how quick it goes from
         * 0 to 1 and then to -1 to 0 etc
         * Math PI * 2 is not used as it increments the growth far too high for the
         * MIC to recognise it.
         */
        float increment = (float) (Math.PI) * frequencyOfTone / sampleRate;
        float angle = 0; // Angle refers to the angular wave

        int samples = duration * sampleRate;

        double[] wave = new double[samples];
        byte[] sound = new byte[2 * samples];

        // fill out the array with samples
        for (int counter = 0; counter < samples; counter++) {

            // The wave variable is the final result of the sine wave, with the
            // specified frequency
            wave[counter] = Math.sin(angle);

            // The angle variable when incremented helps compute the smooth
            // oscillation of the wave
            angle += increment;
        }

        int i;
        int idx = 0;
        int ramp = samples / 20 ;


        // Helps smooth the volume transition to ensure clicks are not heard same with ramp down below
        // Ramp amplitude up (to avoid clicks)
        for (i = 0; i< ramp; ++i) {
            double dVal = wave[i];
            // Ramp up to maximum
            final short val = (short) ((dVal * 32767 * i/ramp));
            // in 16 bit wav PCM, first byte is the low order byte
            sound[idx++] = (byte) (val & 0x00ff);
            sound[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        for (; i < samples - ramp; ++i) {
            double dVal = wave[i];
            final short val = (short) ((dVal * 32767));
            // The sound array is in essence the wave array, but it manipulates the volume
            // to provide the necessary decibels and also it is converted into bytes.
            sound[idx++] = (byte) (val & 0x00ff);
            sound[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        // Ramp amplitude down
        for (; i < samples ; ++i) {
            double dVal = wave[i];
            // Ramp down to zero
            final short val = (short) ((dVal * 32767 * (samples-i)/ramp ));
            // in 16 bit wav PCM, first byte is the low order byte
            sound[idx++] = (byte) (val & 0x00ff);
            sound[idx++] = (byte) ((val & 0xff00) >>> 8);
        }

        return sound;
    }

    /**
     * Writes the parameter byte array to an AudioTrack and plays the array
     *
     */
    private void playSound(@NotNull byte [] sound) {
        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, sound.length, AudioTrack.MODE_STATIC);
        mAudioTrack.write(sound, 0, sound.length);
        mAudioTrack.setVolume(volume);
        mAudioTrack.play();
    }

    @Override
    public void run() {
        super.run();

        if (!isComplete){
            if (mGeneratedTone != null){
                playSound(mGeneratedTone);

            }else {
                Log.e("Project", "mGeneratedTone is null");
            }
        }

    }
}
